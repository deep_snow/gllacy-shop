# Магазин мороженого «Глейси»

*Проект импортирован с [GitHub](https://github.com/dpsnow/574945-gllacy).*

Проект выполнен в рамках интенсива «[Профессиональный HTML и CSS, уровень 1»](https://htmlacademy.ru/intensive/htmlcss) (26 февраля — 4 апреля 2018) от [HTML Academy](https://htmlacademy.ru).

Сверстаны 2 страницы: [главная](https://deep_snow.gitlab.io/gllacy-shop/index.html) и [каталог](https://deep_snow.gitlab.io/gllacy-shop/catalog.html).

- Сетка на flexbox.
- Наименование стилей по БЭМ.
- Знакомство с JS (работа с DOM, валидация формы).
